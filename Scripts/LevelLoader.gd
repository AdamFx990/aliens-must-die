extends Node2D
export var player = "res://Scenes/Characters/Player.tscn"
export var enemy = "res://Scenes/Characters/Enemy.tscn"

const spawn_rate = 1
var time_elapsed = 0
var rift_score = 20

func _ready():
	instantiate_player()

func _process(delta):
	time_elapsed += delta
	spawn_enemy()
	update_rift_score()

func spawn_enemy():
	if (time_elapsed >= spawn_rate):
		time_elapsed = 0
		var enemy_spawn = load(enemy).instance()
		$InteractiveTerrain.add_child(enemy_spawn)
		enemy_spawn.position = $"Non-InteractiveTerrain/EnemySpawn0".position

func update_rift_score():
	$RiftScore.text = "Rift Points: %s" % rift_score

func instantiate_player():
	# This sets the player to appear at the correct area when loading into a new
	# zone
	var spawn_points = $"Non-InteractiveTerrain".get_children()
	var index = GameData.zone_load_spawn_point

	# If we somehow don't have that spawn point, fall back to 0
	if not spawn_points[index]:
		index = 0


	# Spawn the player and add to scene
	var player_spawn = load(player).instance()
	$InteractiveTerrain.add_child(player_spawn)
	# Set player at the correct position (spawn point of zone)
	player_spawn.position = spawn_points[index].position
	# Make the player face the direction from last movement to create a
	# "seamless" feel
	if GameData.zone_load_facing_direction:
		player_spawn.update_facing(GameData.zone_load_facing_direction)
