extends "res://Scripts/Actor.gd"

var movement_rate = 1
var time_elapsed = 0

var DIR_V = {
	"up": Vector2(0, -1),
	"down": Vector2(0, 1),
	"left": Vector2(-1, 0),
	"right": Vector2(1, 0)
}

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time_elapsed += delta
	if (time_elapsed > movement_rate):
		return
	if (move()):
		time_elapsed = 0

func move():
	var movement_vector
	if (dir == DIR.DOWN):
		movement_vector = DIR_V.down
	elif (dir == DIR.UP):
		movement_vector = DIR_V.up
	elif (dir == DIR.LEFT):
		movement_vector = DIR_V.left
	elif (dir == DIR.RIGHT):
		movement_vector = DIR_V.right

	if (!target_position(movement_vector)):
		find_path()
		return false
	return true


# Doom strat (Hug the left wall)
func find_path():
	if (dir == DIR.DOWN):
		update_facing(DIR_V.right)
	elif (dir == DIR.UP):
		update_facing(DIR_V.left)
	elif (dir == DIR.LEFT):
		update_facing(DIR_V.down)
	elif (dir == DIR.RIGHT):
		update_facing(DIR_V.up)
